json.array!(@alumnos) do |alumno|
  json.extract! alumno, :id, :Nombre, :Apellido_Paterno, :Apellido_Materno, :Fecha_De_Nacimiento, :Sexo, :Categoria
  json.url alumno_url(alumno, format: :json)
end
