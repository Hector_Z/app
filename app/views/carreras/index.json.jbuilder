json.array!(@carreras) do |carrera|
  json.extract! carrera, :id, :alumno_id, :Carrera
  json.url carrera_url(carrera, format: :json)
end
