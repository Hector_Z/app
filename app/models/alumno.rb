class Alumno < ActiveRecord::Base
	has_many :carreras, dependent: :destroy
	validates_presence_of :Nombre
	validates_presence_of :Apellido_Paterno
	validates_presence_of :Apellido_Materno
	validates_presence_of :Fecha_De_Nacimiento
	validates_presence_of :Sexo
	validates_presence_of :Categoria
	belongs_to :user
end
