class CreateAlumnos < ActiveRecord::Migration
  def change
    create_table :alumnos do |t|
      t.string :Nombre
      t.string :Apellido_Paterno
      t.string :Apellido_Materno
      t.date :Fecha_De_Nacimiento
      t.string :Sexo
      t.string :Categoria

      t.timestamps null: false
    end
  end
end
